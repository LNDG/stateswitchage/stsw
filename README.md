[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.14221999.svg)](https://doi.org/10.5281/zenodo.14221999)
[![made-with-datalad](https://www.datalad.org/badges/made_with.svg)](https://datalad.org)

# **StateSwitch Ageing**

These DataLad datasets are hosted in two decentralized locations:

On [GitLab](https://git.mpib-berlin.mpg.de/LNDG/stateswitchage/stsw) (```mpibgit``` remote):
- This is a full copy of all StateSwitch Ageing sudatasets, but without support for annexed data. All annexed files (including first and foremost the data) are stored on currently private gin repositories. Once sharing permissions are figured out (incl. setting repos to public in an automated fashion, annexed data should be able to be pulled from the gin repos.)

On [Gin](https://gin.g-node.org/StateSwitch/stsw) (```gin``` remote):
- This is a complete copy including all of the annexed content.

In both cases, the core structure (excluding the annexed files unless explicitly specified) can be retrieved by installing datalad and running 

```
datalad install -r <url>
```

Because some subdatasets have grown a bit large despite annexing, this may take a few hours. Maybe this can be further optimized in the future.

---

The structure relies on a heavy use of dataset nesting. DataLad's strength lies in the support of recursive procedures across subdatasets.
If you would like to check the integrity of files across all subdatasets, the following can come in handy:

```
git clone https://github.com/jkosciessa/recursive_git
[add to bash path]
git recursive annex fsck -q
```

---

# **DataLad datasets and how to use them**

This repository is a [DataLad](https://www.datalad.org/) dataset. It provides
fine-grained data access down to the level of individual files, and allows for
tracking future updates. In order to use this repository for data retrieval,
[DataLad](https://www.datalad.org/) is required. It is a free and
open source command line tool, available for all major operating
systems, and builds up on Git and [git-annex](https://git-annex.branchable.com/)
to allow sharing, synchronizing, and version controlling collections of
large files. You can find information on how to install DataLad at
[handbook.datalad.org/en/latest/intro/installation.html](http://handbook.datalad.org/en/latest/intro/installation.html).

### Get the dataset

A DataLad dataset can be `cloned` by running

```
datalad clone <url>
```

Once a dataset is cloned, it is a light-weight directory on your local machine.
At this point, it contains only small metadata and information on the
identity of the files in the dataset, but not actual *content* of the
(sometimes large) data files.

### Retrieve dataset content

After cloning a dataset, you can retrieve file contents by running

```
datalad get <path/to/directory/or/file>`
```

This command will trigger a download of the files, directories, or
subdatasets you have specified.

DataLad datasets can contain other datasets, so called *subdatasets*.
If you clone the top-level dataset, subdatasets do not yet contain
metadata and information on the identity of files, but appear to be
empty directories. In order to retrieve file availability metadata in
subdatasets, run

```
datalad get -n <path/to/subdataset>
```

Afterwards, you can browse the retrieved metadata to find out about
subdataset contents, and retrieve individual files with `datalad get`.
If you use `datalad get <path/to/subdataset>`, all contents of the
subdataset will be downloaded at once.

### Stay up-to-date

DataLad datasets can be updated. The command `datalad update` will
*fetch* updates and store them on a different branch (by default
`remotes/origin/master`). Running

```
datalad update --merge
```

will *pull* available updates and integrate them in one go.

### Find out what has been done

DataLad datasets contain their history in the ``git log``.
By running ``git log`` (or a tool that displays Git history) in the dataset or on
specific files, you can find out what has been done to the dataset or to individual files
by whom, and when.

### More information

More information on DataLad and how to use it can be found in the DataLad Handbook at
[handbook.datalad.org](http://handbook.datalad.org/en/latest/index.html). The chapter
"DataLad datasets" can help you to familiarize yourself with the concept of a dataset.
